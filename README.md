## Semantic segmentation

Perception system is the first and the foremost component of an autonomous vehicle (AV) stack. As part of perception, in earlier projects, we studied traffic sign recognition based on deep neural networks, and detection of other vehicles using traditional computer vision and machine learning, as well as deep learning based object detection. In terms of complexity of deep learning, image recognition is a classification problem wherein the prediction is at the level of an image; object detection is next level of complexity, where we not only recognize one or more objects in an image, but also their locations in terms of bounding boxes. In this project, we tackle a problem of finding free space on the road to drive the AV, and in terms of complexity, this is the highest level because this requires pixel-level granularity, which in turn improves decision making ability. This problem falls under the category of *semantic segmentation*, where we assign each pixel in the input image a semantic class in order to get a pixel-wise dense classification. The goal of this project is to solve binary classification problem of classifying each pixel as being road or not road using semantic segmentation based on deep learning. The technique can be extended to more classes like road, vehicle, bicycle, pedestrian, etc.

## Repository

The original Udacity project instructions can be found [here](https://github.com/udacity/CarND-Semantic-Segmentation).

## Contents

1. Folder **python_code** contains main.py, helper.py, and project_tests.py.
2. Folder **inference_images** contains test images with segmentation mask.
3. Folder **videos** contains project video with segmentation mask and video made from segmented test images.
4. Detailed summarizing the background, methods, data, and results in _Semantic_segmentation_writeup.ipynb_.

## Results

We found the best results with following sets of parameters:
* keep probability: 0.75
* learning rate: 0.0001
* num epochs: 80
* batch size: 6

[imageA]: ./figures/Metrics.jpg "Metrics"
![alt text][imageA]
**Figure A Training and validation data performance**

Figure B illustrates the segmentation performance on a test example after 5, 10, 25, 50, 60, and 80 epochs of training. The performance improves with the number of epochs upto a certain point, beyond which the performance tends to saturate.

[imageB]: ./figures/Number_of_epochs.jpg "Number of epochs"
![alt text][imageB]
**Figure B Effect of the amount of training on inference**

Figure C illustrates the impact of normalization and data augmentation on segmentation performance. Normalizing the data resulted in jagged boundaries, whereas data augmentation resulted in slightly better performance at the cost of much higher training time.

[imageC]: ./figures/Effect_of_nor_aug.jpg "Impact of normalization and data augmentation"
![alt text][imageC]
**Figure C Impact of normalization and data augmentation**

Please see below the performance of semantic segmentation algorithm applied to a video.

![](./videos/project_video_segmented.mp4)

## Detailed writeup

Detailed report can be found in [_Semantic_segmentation_writeup.md_](Semantic_segmentation_writeup.md).

## Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. The notes provided by Udacity from Pierluigi Ferrari were very handy. Special thanks to Aaron Brown and Brok Bucholtz for their helpful project walkthrough.

