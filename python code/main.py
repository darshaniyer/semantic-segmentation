#!/usr/bin/env python3
import os.path
import tensorflow as tf
import numpy as np
import scipy.misc
import helper
import warnings
import pickle
from itertools import product
import matplotlib.pyplot as plt
from distutils.version import LooseVersion
import project_tests as tests

# Check TensorFlow Version
assert LooseVersion(tf.__version__) >= LooseVersion('1.0'), 'Please use TensorFlow version 1.0 or newer.  You are using {}'.format(tf.__version__)
print('TensorFlow Version: {}'.format(tf.__version__))

# Check for a GPU
if not tf.test.gpu_device_name():
    warnings.warn('No GPU found. Please use a GPU to train your neural network.')
else:
    print('Default GPU Device: {}'.format(tf.test.gpu_device_name()))


def load_vgg(sess, vgg_path):
    """
    Load Pretrained VGG Model into TensorFlow.
    :param sess: TensorFlow Session
    :param vgg_path: Path to vgg folder, containing "variables/" and "saved_model.pb"
    :return: Tuple of Tensors from VGG model (image_input, keep_prob, layer3_out, layer4_out, layer7_out)
    """

    vgg_tag = 'vgg16'
    vgg_input_tensor_name = 'image_input:0'
    vgg_keep_prob_tensor_name = 'keep_prob:0' # forcing it to learn more and avoid overfitting
    vgg_layer3_out_tensor_name = 'layer3_out:0'
    vgg_layer4_out_tensor_name = 'layer4_out:0'
    vgg_layer7_out_tensor_name = 'layer7_out:0'

    tf.saved_model.loader.load(sess, [vgg_tag], vgg_path) # load the graph from file
    graph = tf.get_default_graph() # grab the graph
    image_input = graph.get_tensor_by_name(vgg_input_tensor_name) # from the graph, grab each layer by its name
    keep_prob = graph.get_tensor_by_name(vgg_keep_prob_tensor_name)
    layer3_out = graph.get_tensor_by_name(vgg_layer3_out_tensor_name)    
    layer4_out = graph.get_tensor_by_name(vgg_layer4_out_tensor_name)
    layer7_out = graph.get_tensor_by_name(vgg_layer7_out_tensor_name)
    
    return image_input, keep_prob, layer3_out, layer4_out, layer7_out

tests.test_load_vgg(load_vgg, tf)

# layers() is where we create the architecture for the project
#
# here we complete the final part of encoder which is 1x1 convolution first
# we take our input to decoder which is the final frozen layer vgg_layer7_out
# and upsample by 2
# num_classes = 2 since we are doing binary classification (road, no_road)
# without kernel_regularizer, the weights will become too large and prone to
# overfitting
def layers(vgg_layer3_out, vgg_layer4_out, vgg_layer7_out, num_classes):
    """
    Create the layers for a fully convolutional network.  Build skip-layers using the vgg layers.
    :param vgg_layer3_out: TF Tensor for VGG Layer 3 output
    :param vgg_layer4_out: TF Tensor for VGG Layer 4 output
    :param vgg_layer7_out: TF Tensor for VGG Layer 7 output
    :param num_classes: Number of classes to classify (binary classification of road Vs no road)
    :return: The Tensor for the last layer of output
    """

    # The pretrained VGG is already fully convolutionalized with 1x1 conv layers that replaces the fully connected layers.
    # Those 1x1 convs are the ones used to preserve the spatial information
    # 1x1 conv added on top of the pretrained VGG as below is to reduce the number of filters from 4096 
    # to the number of classes for our specific model.
    # without regularizer, the weights will become too large, and the output will be garbage
    vgg_layer7_out_reshaped = tf.layers.conv2d(vgg_layer7_out, num_classes, 1, # kernel size = 1x1
                                padding='same', kernel_initializer= tf.random_normal_initializer(stddev=0.01),
                                kernel_regularizer = tf.contrib.layers.l2_regularizer(1e-3))

    # Upsample by 2
    vgg_layer7_out_reshaped_upsampled = tf.layers.conv2d_transpose(vgg_layer7_out_reshaped, num_classes, 4, # kernel size = 4x4
                                        2, # stride = (2,2) for upsampling by 2
                                        padding = 'same', kernel_initializer= tf.random_normal_initializer(stddev=0.01),
                                        kernel_regularizer = tf.contrib.layers.l2_regularizer(1e-3))

    # The output of the pooling layer is scaled before it is fed to 1x1 convolutions
    vgg_layer4_out_scaled = tf.multiply(vgg_layer4_out, 0.01, name = 'pool4_out_scaled')
    # The 1x1 convolutions to change the number of channels to num_classes
    vgg_layer4_out_reshaped = tf.layers.conv2d(vgg_layer4_out_scaled, num_classes, 1, # kernel size = 1x1
                                 padding='same', kernel_initializer= tf.random_normal_initializer(stddev=0.01),
                                 kernel_regularizer = tf.contrib.layers.l2_regularizer(1e-3))

    # Add a skip connection between the reshaped layer 7 output and reshaped layer 4 output
    fuse_7_4 = tf.add(vgg_layer7_out_reshaped_upsampled, vgg_layer4_out_reshaped) # adding a skip connection

    # Upsample by 2
    fuse_7_4_upsampled = tf.layers.conv2d_transpose(fuse_7_4, num_classes, 4, # kernel size = 4x4
                                        2, # stride = (2,2) for upsampling by 2
                                        padding = 'same', kernel_initializer= tf.random_normal_initializer(stddev=0.01),
                                        kernel_regularizer = tf.contrib.layers.l2_regularizer(1e-3))

    # The output of the pooling layer is scaled before it is fed to 1x1 convolutions
    vgg_layer3_out_scaled = tf.multiply(vgg_layer3_out, 0.0001, name='pool3_out_scaled')
    # The 1x1 convolutions to change the number of channels to num_classes
    vgg_layer3_out_reshaped = tf.layers.conv2d(vgg_layer3_out_scaled, num_classes, 1, # kernel size = 1x1
                                padding='same', kernel_initializer= tf.random_normal_initializer(stddev=0.01),
                                kernel_regularizer = tf.contrib.layers.l2_regularizer(1e-3))

    # Add a skip connection between the Upsampled output and reshaped layer 3 output
    fuse_7_4_3 = tf.add(fuse_7_4_upsampled, vgg_layer3_out_reshaped) # adding a skip connection

    # Upsample by 8
    fuse_7_4_3_upsampled = tf.layers.conv2d_transpose(fuse_7_4_3, num_classes, 16, # kernel size = 16x16
                                        8, # stride = (8,8) for upsampling by 8
                                        padding = 'same', kernel_initializer= tf.random_normal_initializer(stddev=0.01),
                                        kernel_regularizer = tf.contrib.layers.l2_regularizer(1e-3))

    return fuse_7_4_3_upsampled # final output is of the same size as our input image
tests.test_layers(layers)


def optimize(nn_last_layer, correct_label, learning_rate, num_classes):
    """
    Build the TensorFLow loss and optimizer operations.
    :param nn_last_layer: TF Tensor of the last layer in the neural network
    :param correct_label: TF Placeholder for the correct label image
    :param learning_rate: TF Placeholder for the learning rate
    :param num_classes: Number of classes to classify
    :return: Tuple of (softmax, train_op, loss_op, accuracy_op, mean_iou, mean_iou_op)
    """
    # TensorFlow’s softmax function (and tf.nn.softmax_cross_entropy_with_logits) 
    # accepts tensors of any shape and will apply the softmax function on the 
    # last axis of the tensor. Consequently, there is no need to reshape the output of the model.

    # reshape last layer of decoder to logits
    logits = tf.reshape(nn_last_layer, (-1, num_classes)) # pixel index Vs classes

    # reshape the ground truth labels to the shape of logits
    correct_label_reshaped = tf.reshape(correct_label, (-1, num_classes))

    # apply softmax to logits to calculate probabilities
    softmax = tf.nn.softmax(logits, name = 'softmax')

    # calculate cross entropy between the logits and ground truth labels
    cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(labels=correct_label_reshaped, logits=logits)

    # calculate mean cross entropy loss
    mean_cross_entropy = tf.reduce_mean(cross_entropy)

    # apply regularization explicitly to the cross entropy loss to create regularized loss operation
    reg_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES) # list of the individual loss values
    reg_constant = 1
    loss_op = mean_cross_entropy + reg_constant * sum(reg_losses)

    # setup the optimizer
    optimizer = tf.train.AdamOptimizer(learning_rate = learning_rate)

    # minimize the regularized cross entropy loss
    train_op = optimizer.minimize(loss_op)

    # calculate correct predictions
    correct_prediction = tf.equal(tf.argmax(logits, 1), tf.argmax(correct_label_reshaped, 1))

    # calculate accuracy as the mean of correct predictions
    accuracy_op = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    # calculate mean IOU
    mean_iou, mean_iou_op = \
        tf.metrics.mean_iou(tf.argmax(correct_label_reshaped, 1), tf.argmax(logits, 1), num_classes)

    return softmax, train_op, loss_op, accuracy_op, mean_iou, mean_iou_op
tests.test_optimize(optimize)

# calculate the cross entropy loss, accuracy, and mean IOU for the validation data
def validate(sess, batch_size, get_batches_val,
             loss_op, accuracy_op, mean_iou, mean_iou_op,
             input_image, correct_label, keep_prob, learning_rate, kp, lr):

    total_accuracy = 0
    total_ce_loss = 0
    total_m_iou = 0
    ctr = 0
    for image_batch, label_batch in get_batches_val(batch_size):  # loop through batches
        (loss, acc, iou, _) = sess.run([loss_op, accuracy_op, mean_iou, mean_iou_op],
                                            feed_dict={input_image: image_batch, correct_label: label_batch,
                                                       keep_prob: kp, learning_rate: lr})

        total_ce_loss += loss * len(image_batch)
        total_accuracy += acc * len(image_batch)
        total_m_iou += iou * len(image_batch)
        ctr += len(image_batch)

    ce_loss = total_ce_loss / ctr
    accuracy = total_accuracy / ctr
    m_iou = total_m_iou / ctr

    return ce_loss, accuracy, m_iou

# get_batches_train returns an image and its label pair, based on batch size
def train_nn(sess, num_epochs, batch_size, get_batches_train,
                        train_op, loss_op, accuracy_op, mean_iou, mean_iou_op, input_image,
                        correct_label, keep_prob, learning_rate, kp, lr):
    """
    Train neural network and print out the loss during training.
    :param sess: TF Session
    :param num_epochs: Number of epochs
    :param batch_size: Batch size
    :param get_batches_train: Function to get batches of training data. Call using get_batches_train(batch_size)
    :param train_op: TF Operation to train the neural network
    :param loss_op: TF Tensor for the amount of loss
    :param accuracy_op: TF Tensor for the accuracy
    :param mean_iou: TF Tensor for the mean IOU
    :param input_image: TF Placeholder for input images
    :param correct_label: TF Placeholder for ground truth labels
    :param keep_prob: TF Placeholder for dropout keep probability
    :param learning_rate: TF Placeholder for learning rate
    :param kp: value of keep probability to be supplied to the keep_prob placeholder
    :param lr: value of learning rate to be supplied to the learning rate placeholder
    """
    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer()) # for mean IOU

    ce_loss_train = []
    accuracy_train = []
    m_iou_train = []

    print("Training...")
    for i in range(num_epochs): # loop through epochs
        total_accuracy = 0
        total_ce_loss = 0
        total_m_iou = 0
        ctr = 0
        for image_batch, label_batch in get_batches_train(batch_size): # loop through batches
            (loss, acc, iou, t1, t2) = sess.run([loss_op, accuracy_op, mean_iou, train_op, mean_iou_op],
                                       feed_dict={input_image: image_batch, correct_label: label_batch,
                                                  keep_prob: kp, learning_rate: lr})

            total_ce_loss += loss * len(image_batch)
            total_accuracy += acc * len(image_batch)
            total_m_iou += iou * len(image_batch)
            ctr += len(image_batch)

        ce_loss_train.append(total_ce_loss / ctr)
        accuracy_train.append(total_accuracy / ctr)
        m_iou_train.append(total_m_iou / ctr)

        print("EPOCH (train): {} ".format(i+1) + "  CE loss: {} ".format(round(ce_loss_train[i],2)) +
              " Accuracy: {} ".format(round(accuracy_train[i],2)) + "Mean IOU: {} ".format(round(m_iou_train[i],2)))

    print("Training done...")

    return
# tests.test_train_nn(train_nn)

def train_validate_test(sess, num_epochs, batch_size, get_batches_train, get_batches_val,
                        train_op, loss_op, accuracy_op, mean_iou, mean_iou_op, input_image,
                        correct_label, keep_prob, learning_rate, kp, lr,
                        softmax, runs_dir, data_dir, normalize_flag, image_shape):
    """
    Train neural network and print out the loss during training.
    :param sess: TF Session
    :param num_epochs: Number of epochs
    :param batch_size: Batch size
    :param get_batches_train: Function to get batches of training data. Call using get_batches_train(batch_size)
    :param get_batches_val: Function to get batches of validation data. Call using get_batches_val(batch_size)
    :param train_op: TF Operation to train the neural network
    :param loss_op: TF Tensor for the amount of loss
    :param accuracy_op: TF Tensor for the accuracy
    :param mean_iou: TF Tensor for the mean IOU
    :param input_image: TF Placeholder for input images
    :param correct_label: TF Placeholder for ground truth labels
    :param keep_prob: TF Placeholder for dropout keep probability
    :param learning_rate: TF Placeholder for learning rate
    :param kp: value of keep probability to be supplied to the keep_prob placeholder
    :param lr: value of learning rate to be supplied to the learning rate placeholder
    :param runs_dir: directory to store the processed test images (test image + segmentation mask)
    :param softmax: directory containing the test images
    :param normalize_flag: boolean flag that decides whether to apply normalization to test images
    :param image_shape: tuple containing the shape to which the test images are resized
    """

    num_epochs_coll = [5, 10, 25, 40, 50, 60, 80, 100]

    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer())

    ce_loss_train = []
    accuracy_train = []
    m_iou_train = []

    ce_loss_val = []
    accuracy_val = []
    m_iou_val = []

    print("Training...")
    for i in range(num_epochs): # loop through epochs
        total_accuracy = 0
        total_ce_loss = 0
        total_m_iou = 0
        ctr = 0
        for image_batch, label_batch in get_batches_train(batch_size): # loop through training batches
            (loss, acc, iou, t1, t2) = sess.run([loss_op, accuracy_op, mean_iou, train_op, mean_iou_op],
                                       feed_dict={input_image: image_batch, correct_label: label_batch,
                                                  keep_prob: kp, learning_rate: lr})

            total_ce_loss += loss * len(image_batch)
            total_accuracy += acc * len(image_batch)
            total_m_iou += iou * len(image_batch)
            ctr += len(image_batch)

        ce_loss_train.append(total_ce_loss / ctr)
        accuracy_train.append(total_accuracy / ctr)
        m_iou_train.append(total_m_iou / ctr)

        # print training metrics at the (i+1)th epoch
        print("EPOCH (train): {} ".format(i+1) + "  CE loss: {} ".format(round(ce_loss_train[i],2)) +
              " Accuracy: {} ".format(round(accuracy_train[i],2)) + "Mean IOU: {} ".format(round(m_iou_train[i],2)))

        # apply the trained model after 'i+1' epochs to validation data
        c, a, m = \
            validate(sess, batch_size, get_batches_val,
                     loss_op, accuracy_op, mean_iou, mean_iou_op,
                     input_image, correct_label, keep_prob, learning_rate, kp, lr)

        ce_loss_val.append(c)
        accuracy_val.append(a)
        m_iou_val.append(m)

        # print validation metrics at the (i+1)th epoch
        print("EPOCH (valid): {} ".format(i+1) + "  CE loss: {} ".format(round(ce_loss_val[i],2)) +
              " Accuracy: {} ".format(round(accuracy_val[i],2)) + "Mean IOU: {} ".format(round(m_iou_val[i],2)))

        # perform inference after specific number of epochs
        if (i+1 in num_epochs_coll):
            print('Starting inference ... ')
            helper.save_inference_samples(runs_dir, data_dir, sess, image_shape, softmax, keep_prob,
                                          input_image, normalize_flag)
            print('Inference done ... ')

    print("Training and validation done...")

    # plot the training and validation metrics
    plt.figure()
    f, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(2, 3, figsize=(21, 12))
    f.tight_layout()
    fsize = 12

    ax1.plot(ce_loss_train, '.')
    ax1.set_title('CE loss (train)', fontsize=fsize)
    ax1.tick_params(labelsize=fsize)
    ax1.grid()

    ax2.plot(accuracy_train, 'r.')
    ax2.set_title('Accuracy (train)', fontsize=fsize)
    ax2.tick_params(labelsize=fsize)
    ax2.grid()

    ax3.plot(m_iou_train, 'k.')
    ax3.set_title('Mean IOU (train)', fontsize=fsize)
    ax3.tick_params(labelsize=fsize)
    ax3.grid()

    ax4.plot(ce_loss_val, '.')
    ax4.set_title('CE loss (val)', fontsize=fsize)
    ax4.set_xlabel('Epoch', fontsize=fsize)
    ax4.tick_params(labelsize=fsize)
    ax4.grid()

    ax5.plot(accuracy_val, 'r.')
    ax5.set_title('Accuracy (val)', fontsize=fsize)
    ax5.set_xlabel('Epoch', fontsize=fsize)
    ax5.tick_params(labelsize=fsize)
    ax5.grid()

    ax6.plot(m_iou_val, 'k.')
    ax6.set_title('Mean IOU (val)', fontsize=fsize)
    ax6.set_xlabel('Epoch', fontsize=fsize)
    ax6.tick_params(labelsize=fsize)
    ax6.grid()

    save_file = 'Metrics.png'
    plt.savefig(save_file, bbox_inches='tight', orientation='landscape', dpi=300)

    return

# perform inference on a single image from a video
def infer(image):

    global image_shape
    global normalize_flag
    global input_image
    global keep_prob
    global softmax

    # resize the image
    image = scipy.misc.imresize(image, image_shape)

    # normalize the resized imaged depending on the state of normalization flag
    if (normalize_flag):
        image = image.astype('float32')
        image /= 255.0
        image -= 0.5

    sess = tf.get_default_session()
    # calculate softmax probabilities for the input image
    probs = sess.run([softmax], feed_dict={input_image: [image], keep_prob: 1.0})
    # reshape the probabilities to the shape of the input image
    im_softmax = probs[0][:, 1].reshape(image_shape[0], image_shape[1])
    # find the max probability class and reshape the resulting output
    segmentation = (im_softmax > 0.5).reshape(image_shape[0], image_shape[1], 1)
    # generate a green colored segmentation mask from the segmentation result
    mask = np.dot(segmentation, np.array([[0, 255, 0, 127]]))
    # convert the mask to RGBA image in IMAGE format
    mask = scipy.misc.toimage(mask, mode="RGBA")
    # convert the input image array to IMAGE format
    street_im = scipy.misc.toimage(image)
    # overlay the segmentation mask on the input image
    street_im.paste(mask, box=None, mask=mask)

    return np.array(street_im) # convert the segmented image into numpy array and return

def apply_to_video(video_file, result_file, checkpoint_file):

    global input_image
    global keep_prob
    global softmax

    from moviepy.editor import VideoFileClip
    from IPython.display import HTML

    HTML("""
    <video width="960" height="600" controls>
      <source src="{0}" type="video/mp4">
    </video>
    """.format(video_file))

    # Extract the clip from the video file
    original_clip = VideoFileClip(video_file)

    # Create a TensorFlow configuration object.
    # This will be passed as an argument to the session.
    config = tf.ConfigProto()
    # JIT level, this can be set to ON_1 or ON_2
    jit_level = tf.OptimizerOptions.ON_1
    config.graph_options.optimizer_options.global_jit_level = jit_level

    saver = tf.train.Saver()
    with tf.Session(config=config) as sess:
        saver.restore(sess, checkpoint_file) # restore the saved session from the checkpoint file
        input_image = sess.graph.get_tensor_by_name('image_input:0') # get input image placeholder
        keep_prob = sess.graph.get_tensor_by_name('keep_prob:0') # get keep probability placeholder
        softmax = sess.graph.get_tensor_by_name('softmax:0') # get softmax tensor

        # perform inference on each frame of the clip, and
        # glue the segmented frames to generated a processed clip
        processed_clip = original_clip.fl_image(infer)

        # write to file
        processed_clip.write_videofile(result_file, audio=False)
  
# run() is the wrapper function
def run():

    global image_shape
    global normalize_flag

    os.chdir('.')
    base_path = os.path.abspath('.')
    data_dir = os.path.join(base_path, 'data')
    video_dir = os.path.join(base_path, 'video')
    model_dir = os.path.join(base_path, 'model')
    runs_dir = os.path.join(base_path, 'runs')

    # split the data into training and validation sets
    image_shape = (160, 576)
    # helper.collect_data(os.path.join(data_dir, 'data_road/training'), image_shape,
    #                     os.path.join(data_dir, 'data_road/data_bin_train.p'),
    #                     os.path.join(data_dir, 'data_road/data_bin_val.p'),
    #                     tertiary_label=False, augmentation_flag=False)
    # helper.collect_data(os.path.join(data_dir, 'data_road/training'), image_shape,
    #                     os.path.join(data_dir, 'data_road/data_bin_aug_train.p'),
    #                     os.path.join(data_dir, 'data_road/data_bin_aug_val.p'),
    #                     tertiary_label=False, augmentation_flag=True)
    # helper.collect_data(os.path.join(data_dir, 'data_road/training'), image_shape,
    #                     os.path.join(data_dir, 'data_road/data_tri_train.p'),
    #                     os.path.join(data_dir, 'data_road/data_tri_val.p'),
    #                     tertiary_label=True, augmentation_flag=False)
    # helper.collect_data(os.path.join(data_dir, 'data_road/training'), image_shape,
    #                     os.path.join(data_dir, 'data_road/data_tri_aug_train.p'),
    #                     os.path.join(data_dir, 'data_road/data_tri_aug_val.p'),
    #                     tertiary_label=True, augmentation_flag=True)

    tests.test_for_kitti_dataset(data_dir)

    # the following three lines help prevent out of memory (OOM) error
    tf.reset_default_graph()
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    with tf.Session(config=config) as sess:
        # Path to vgg model
        vgg_path = os.path.join(model_dir, 'vgg')

        num_classes = 2 # road Vs non-road

        normalize_flag = False # normalize the data
        augmentation_flag = False # augment the training and validation data
        if (num_classes == 3):
            tertiary_label = True # process three labels
            if (augmentation_flag == True):
                train_file = 'data_road/data_tri_aug_train.p'
                val_file = 'data_road/data_tri_aug_val.p'
            else:
                train_file = 'data_road/data_tri_train.p'
                val_file = 'data_road/data_tri_val.p'
        else:
            tertiary_label = False  # process two labels
            if (augmentation_flag == True):
                train_file = 'data_road/data_bin_aug_train.p'
                val_file = 'data_road/data_bin_aug_val.p'
            else:
                train_file = 'data_road/data_bin_train.p'
                val_file = 'data_road/data_bin_val.p'

        # Create function to get training and validation batches
        get_batches_train = helper.gen_batch_simple_function(os.path.join(data_dir, train_file), normalize_flag)
        get_batches_val = helper.gen_batch_simple_function(os.path.join(data_dir, val_file), normalize_flag)

        # Build NN using load_vgg, layers, and optimize function

        # Extract relevant encoder layers from the VGG model trained on ImageNet data
        input_image, keep_prob, layer3_out, layer4_out, layer7_out = load_vgg(sess, vgg_path)

        # Construct a decoder and form a fully convolutional network
        layer_output = layers(layer3_out, layer4_out, layer7_out, num_classes)

        # Create TF placeholder for passing the ground truth labels
        correct_label = tf.placeholder(tf.int32, (None, None, None, num_classes))
        # Create TF placeholder for passing the learning rate
        learning_rate = tf.placeholder(tf.float32)

        # Create tensors for softmax, training, and metrics
        softmax, train_op, loss_op, accuracy_op, mean_iou, mean_iou_op = \
            optimize(layer_output, correct_label, learning_rate, num_classes)

        batch_size = 5
        num_epochs = 80
        kp = 0.75
        lr = 0.0001

        # Train NN
        # train_nn(sess, num_epochs, batch_size, get_batches_train,
        #          train_op, loss_op, accuracy_op, mean_iou, mean_iou_op, input_image,
        #          correct_label, keep_prob, learning_rate, kp, lr)

        # Train NN and validate
        train_validate_test(sess, num_epochs, batch_size, get_batches_train, get_batches_val,
                            train_op, loss_op, accuracy_op, mean_iou, mean_iou_op, input_image,
                            correct_label, keep_prob, learning_rate, kp, lr,
                            softmax, runs_dir, data_dir, normalize_flag, image_shape)

        # Save the model
        print('Saving model ... ')
        saver = tf.train.Saver()
        fcn_path = os.path.join(model_dir, 'fcn')
        save_path = saver.save(sess, os.path.join(fcn_path, 'fcn_model'))
        print("Model saved in path: %s" % save_path)

        # Perform inference on the test data
        print('Starting inference ... ')
        helper.save_inference_samples(runs_dir, data_dir, sess, image_shape, softmax, keep_prob,
                                      input_image, normalize_flag)
        print('Inference done ... ')

        # Process the videos
        print('Processing video ...')
        out_test_video_file = 'test_video_seg_' + str(num_epochs) + '.mp4'
        apply_to_video(os.path.join(video_dir, 'test_video.mp4'), os.path.join(video_dir, out_test_video_file),
                       os.path.join(fcn_path, 'fcn_model'))

        out_project_video_file = 'project_video_seg_' + str(num_epochs) + '.mp4'
        apply_to_video(os.path.join(video_dir, 'project_video.mp4'), os.path.join(video_dir, out_project_video_file),
                       os.path.join(fcn_path, 'fcn_model'))

    print('Done ...')

if __name__ == '__main__':
    run()
