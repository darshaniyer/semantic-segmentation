import re
import random
import numpy as np
import cv2
import os.path
import scipy.misc
import shutil
import zipfile
import time
import pickle
import tensorflow as tf
from glob import glob
from urllib.request import urlretrieve
from tqdm import tqdm
from sklearn.utils import shuffle
from sklearn.cross_validation import train_test_split


class DLProgress(tqdm):
    last_block = 0

    def hook(self, block_num=1, block_size=1, total_size=None):
        self.total = total_size
        self.update((block_num - self.last_block) * block_size)
        self.last_block = block_num


def maybe_download_pretrained_vgg(data_dir):
    """
    Download and extract pretrained vgg model if it doesn't exist
    :param data_dir: Directory to download the model to
    """
    vgg_filename = 'vgg.zip'
    vgg_path = os.path.join(data_dir, 'vgg')
    vgg_files = [
        os.path.join(vgg_path, 'variables/variables.data-00000-of-00001'),
        os.path.join(vgg_path, 'variables/variables.index'),
        os.path.join(vgg_path, 'saved_model.pb')]

    missing_vgg_files = [vgg_file for vgg_file in vgg_files if not os.path.exists(vgg_file)]
    if missing_vgg_files:
        # Clean vgg dir
        if os.path.exists(vgg_path):
            shutil.rmtree(vgg_path)
        os.makedirs(vgg_path)

        # Download vgg
        print('Downloading pre-trained vgg model...')
        with DLProgress(unit='B', unit_scale=True, miniters=1) as pbar:
            urlretrieve(
                'https://s3-us-west-1.amazonaws.com/udacity-selfdrivingcar/vgg.zip',
                os.path.join(vgg_path, vgg_filename),
                pbar.hook)

        # Extract vgg
        print('Extracting model...')
        zip_ref = zipfile.ZipFile(os.path.join(vgg_path, vgg_filename), 'r')
        zip_ref.extractall(data_dir)
        zip_ref.close()

        # Remove zip file to save space
        os.remove(os.path.join(vgg_path, vgg_filename))


def gen_test_output(sess, softmax, keep_prob, image_pl, data_folder, image_shape, normalize_flag):
    """
    Generate test output using the test images
    :param sess: TF session
    :param logits: TF Tensor for the logits
    :param keep_prob: TF Placeholder for the dropout keep probability
    :param image_pl: TF Placeholder for the image placeholder
    :param data_folder: Path to the folder that contains the datasets
    :param image_shape: Tuple - Shape of image
    :return: Output for for each test image
    """
    # prev_mask = []
    for image_file in glob(os.path.join(data_folder, 'image_2', '*.png')):
        image = []
        im_softmax = []
        segmentation = []
        mask = []
        street_im = []
        probs = []

        image = scipy.misc.imresize(scipy.misc.imread(image_file), image_shape)

        if (normalize_flag):
            image = image.astype('float32')
            image /= 255.0
            image -= 0.5

        probs = sess.run([softmax], feed_dict={image_pl: [image], keep_prob: 1.0})
        im_softmax = probs[0][:, 1].reshape(image_shape[0], image_shape[1])
        segmentation = (im_softmax > 0.5).reshape(image_shape[0], image_shape[1], 1)
        mask = np.dot(segmentation, np.array([[0, 255, 0, 127]]))
        mask = scipy.misc.toimage(mask, mode="RGBA")

        street_im = scipy.misc.toimage(image)
        street_im.paste(mask, box=None, mask=mask)

        yield os.path.basename(image_file), np.array(street_im)


def save_inference_samples(runs_dir, data_dir, sess, image_shape, softmax, keep_prob, input_image, normalize_flag = False):
    # Make folder for current run
    output_dir = os.path.join(runs_dir, str(time.time()))
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    os.makedirs(output_dir)

    # Run NN on test images and save them to HD
    print('Saving test images to: {}'.format(output_dir))
    image_outputs = gen_test_output(
        sess, softmax, keep_prob, input_image, os.path.join(data_dir, 'data_road/testing'), image_shape, normalize_flag)
    for name, image in image_outputs:
        scipy.misc.imsave(os.path.join(output_dir, name), image)

# Data augmentation function
def add_salt_pepper_noise(X_imgs, labels):
    # Need to produce a copy as to not modify the original image
    X_imgs_copy = X_imgs.copy()
    row, col, _ = X_imgs_copy[0].shape
    salt_vs_pepper = 0.05
    amount = 0.005
    num_salt = np.ceil(amount * X_imgs_copy[0].size * salt_vs_pepper)
    num_pepper = np.ceil(amount * X_imgs_copy[0].size * (1.0 - salt_vs_pepper))
    for X_img in X_imgs_copy:
        # Add Salt noise
        coords = [np.random.randint(0, i - 1, int(num_salt)) for i in X_img.shape]
        X_img[coords[0], coords[1], :] = 1

        # Add Pepper noise
        coords = [np.random.randint(0, i - 1, int(num_pepper)) for i in X_img.shape]
        X_img[coords[0], coords[1], :] = 0

    return X_imgs_copy, labels

# Data augmentation function
def randomize_color_channel(X_imgs, labels):
    X_imgs_copy = X_imgs.copy()
    row, col, nchan = X_imgs_copy[0].shape
    altered_imgs = []

    for X_img in X_imgs_copy:
        a = X_img[:, :, np.random.permutation(nchan)]
        altered_imgs.append(a)

    return np.array(altered_imgs), labels

# Data augmentation function
def add_gaussian_noise(X_imgs, labels):
    X_imgs_copy = X_imgs.copy()
    X_imgs_copy = X_imgs_copy.astype(np.float32)
    row, col, ch = X_imgs_copy[0].shape
    gaussian_noise_imgs = []

    # Gaussian distribution parameters
    mean = 0
    var = 1
    sigma = var ** 0.5

    for X_img in X_imgs_copy:
        gauss = np.random.normal(mean, sigma, (row, col, ch))
        gaussian = gauss.reshape(row, col, ch).astype(np.float32)
        gaussian_img = cv2.addWeighted(X_img, 0.75, 0.25 * gaussian, 0.25, 0)
        gaussian_noise_imgs.append(gaussian_img)
    gaussian_noise_imgs = np.array(gaussian_noise_imgs, dtype=np.float32)

    return gaussian_noise_imgs, labels

# Data augmentation wrapper function
def apply_augmentation(X_imgs, labels):

    X_sp, y_sp = add_salt_pepper_noise(X_imgs, labels)

    X_cs, y_cs = randomize_color_channel(X_imgs, labels)

    X_gs, y_gs = add_gaussian_noise(X_imgs, labels)

    X_imgs_aug = np.concatenate((X_imgs, X_sp, X_gs, X_cs))
    labels_aug = np.concatenate((labels, y_sp, y_gs, y_cs)).astype('int32')

    return X_imgs_aug, labels_aug


def gen_batch_function(data_folder, image_shape, normalize_flag = False, tertiary_label = False):
    """
    Generate function to create batches of training data
    :param data_folder: Path to folder that contains all the datasets
    :param image_shape: Tuple - Shape of image
    :return:
    """
    def get_batches_fn(batch_size):
        """
        Create batches of training data
        :param batch_size: Batch Size
        :return: Batches of training data
        """
        image_paths = glob(os.path.join(data_folder, 'image_2', '*.png'))
        label_paths = {
            re.sub(r'_(lane|road)_', '_', os.path.basename(path)): path
            for path in glob(os.path.join(data_folder, 'gt_image_2', '*_road_*.png'))}

        background_color = np.array([255, 0, 0])
        secondary_lane = np.array([0, 0, 0])
        primary_lane = np.array([255, 0, 255])

        random.shuffle(image_paths)
        for batch_i in range(0, len(image_paths), batch_size):
            images = []
            gt_images = []
            for image_file in image_paths[batch_i:batch_i+batch_size]:
                gt_image_file = label_paths[os.path.basename(image_file)]

                image = scipy.misc.imresize(scipy.misc.imread(image_file), image_shape)
                gt_image = scipy.misc.imresize(scipy.misc.imread(gt_image_file), image_shape)

                if (tertiary_label):
                    gt_bg = np.all(gt_image == background_color, axis=2)
                    gt_bg = gt_bg.reshape(*gt_bg.shape, 1)
                    gt_sec = np.all(gt_image == secondary_lane, axis=2)
                    gt_sec = gt_sec.reshape(*gt_sec.shape, 1)
                    gt_pri = np.all(gt_image == primary_lane, axis=2)
                    gt_pri = gt_pri.reshape(*gt_pri.shape, 1)
                    gt_image = np.concatenate((gt_bg, gt_sec, gt_pri), axis=2)
                else:
                    gt_bg = np.all(gt_image == background_color, axis=2)
                    gt_bg = gt_bg.reshape(*gt_bg.shape, 1)
                    gt_image = np.concatenate((gt_bg, np.invert(gt_bg)), axis=2)

                gt_image = gt_image.astype(np.int32)

                images.append(image)
                gt_images.append(gt_image)

            X_imgs = np.array(images)
            labels = np.array(gt_images)

            if (normalize_flag):
                X_imgs = X_imgs.astype('float32')
                X_imgs /= 255.0
                X_imgs -= 0.5

            yield X_imgs, labels

    return get_batches_fn

# Split data into training and validation sets and save in respective files
def collect_data(data_folder, image_shape, train_file, val_file, tertiary_label = False, augmentation_flag = False):
    """
    :param data_folder: Folder containing the data
    :param image_shape: shape to which the images in the data_folder will be resized
    :param train_file: file to which the training image and label pairs will be saved
    :param val_file: shape to which the validation image and label pairs will be saved
    :param tertiary_label: flag dictating whether 2 or 3 labels will be extracted from the ground truth images
    :param augmentation_flag: flag dictating whether data augmentation will be performed
    """

    image_paths = glob(os.path.join(data_folder, 'image_2', '*.png'))
    label_paths = {
        re.sub(r'_(lane|road)_', '_', os.path.basename(path)): path
        for path in glob(os.path.join(data_folder, 'gt_image_2', '*_road_*.png'))}

    train_x, val_x, train_y, val_y = \
        train_test_split(np.arange(len(image_paths)), np.arange(len(label_paths)), test_size=0.2, random_state=42)

    image_paths_train = [image_paths[x] for x in train_x]
    image_paths_val = [image_paths[x] for x in val_x]

    collect_data_internal(image_paths_train, label_paths, image_shape, train_file, tertiary_label, augmentation_flag)

    collect_data_internal(image_paths_val, label_paths, image_shape, val_file, tertiary_label, augmentation_flag)

# Internal function to extract the ground truth labels, one hot encode them, and create image, label pairs
def collect_data_internal(image_paths, label_paths, image_shape, save_file, tertiary_label, augmentation_flag):
    """
    :param image_paths: paths containing the data
    :param label_paths: paths containing the ground truth labels
    :param image_shape: shape to which each image will be resized
    :param save_file: file to which the image and label pairs will be saved
    :param tertiary_label: flag dictating whether 2 or 3 labels will be extracted from the ground truth images
    :param augmentation_flag: flag dictating whether data augmentation will be performed
    """

    background_color = np.array([255, 0, 0])
    secondary_lane = np.array([0, 0, 0])
    primary_lane = np.array([255, 0, 255])

    images = []
    gt_images = []

    for image_file in image_paths:
        gt_image_file = label_paths[os.path.basename(image_file)]

        image = scipy.misc.imresize(scipy.misc.imread(image_file), image_shape)
        gt_image = scipy.misc.imresize(scipy.misc.imread(gt_image_file), image_shape)

        if (tertiary_label):
            gt_bg = np.all(gt_image == background_color, axis=2)
            gt_bg = gt_bg.reshape(*gt_bg.shape, 1)
            gt_sec = np.all(gt_image == secondary_lane, axis=2)
            gt_sec = gt_sec.reshape(*gt_sec.shape, 1)
            gt_pri = np.all(gt_image == primary_lane, axis=2)
            gt_pri = gt_pri.reshape(*gt_pri.shape, 1)
            gt_image = np.concatenate((gt_bg, gt_sec, gt_pri), axis=2)
        else:
            gt_bg = np.all(gt_image == background_color, axis=2)
            gt_bg = gt_bg.reshape(*gt_bg.shape, 1)
            gt_image = np.concatenate((gt_bg, np.invert(gt_bg)), axis=2)

        gt_image = gt_image.astype(np.int32)

        images.append(image)
        gt_images.append(gt_image)

    X_imgs = np.array(images)
    labels = np.array(gt_images)

    if (augmentation_flag):
        X_imgs, labels = apply_augmentation(X_imgs, labels)

    save_object = {'data': X_imgs, 'labels': labels}
    with open(save_file, "wb") as f:
        pickle.dump(save_object, f)

    return


def gen_batch_simple_function(data_file, normalize_flag = False):
    """
    Generate function to create batches of training data
    :param data_file: Name of the file containing all the images
    :param normalize_flag: Flag dictating data normalization
    :return:
    """
    def get_batches_fn(batch_size):
        """
        Create batches of training or validation data
        :param batch_size: Batch Size
        :return: Batches of data
        """

        with open(data_file, "rb") as f:
            data_object = pickle.load(f)

        X_imgs_all = data_object['data']
        labels_all = data_object['labels']

        X_imgs_all, labels_all = shuffle(X_imgs_all, labels_all)
        for batch_i in range(0, len(labels_all), batch_size):
            images = []
            gt_images = []
            for image, gt_image in zip(X_imgs_all[batch_i:batch_i+batch_size], labels_all[batch_i:batch_i+batch_size]):
                images.append(image)
                gt_images.append(gt_image)

            X_imgs = np.array(images)
            labels = np.array(gt_images)

            if (normalize_flag):
                X_imgs = X_imgs.astype('float32')
                X_imgs /= 255.0
                X_imgs -= 0.5

            yield X_imgs, labels

    return get_batches_fn




